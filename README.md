# binary-cpp

[https://gitlab.com/king011/go-binary](https://gitlab.com/king011/go-binary) 編碼方案的 c++ 庫

# 宏 選項

在 include binary_cpp.h 之前 可以定義一些 宏來 控制 系統如何運作

* **BINARY\_CPP\_USE\_CC11\_SHARED** 如果被定義 將使用 c++11 的 std::shared_ptr 否則使用 boost::shared_ptr

* **BINARY\_CPP\_DEFAULT\_MAX\_BUFFER** 指定了系統默認支持的 最大編碼長度

* **BINARY\_CPP\_FLOAT32\_T** 默認 被定義爲了 float 如果相應環境下 float 不是32字節 需要預先定義 **BINARY\_CPP\_FLOAT32\_T** 到正確的 32字節 浮點

* **BINARY\_CPP\_FLOAT64\_T** 默認 被定義爲了 double 如果相應環境下 double 不是64字節 需要預先定義 **BINARY\_CPP\_FLOAT64\_T** 到正確的 64字節 浮點

# 使用方法

如果使用 std::shared_ptr 定義 **BINARY\_CPP\_USE\_CC11\_SHARED** 否則 配置好 boost::shared_ptr 環境

1. 將 src/cpp/binary_cpp.h 加入 include 目錄
2. 將 src/cpp/binary_cpp.cpp 加入 build
3. 將 go-binary 生成的 檔案 加入 項目

# Example

```cpp
#include "cmd.h"

// include 核心實現
#include <binary_cpp.h>
// include go-binary 工具創建的 代碼實現
#include <types.h>

void subcommand_example(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("example", "example");
    cmd->callback([&cmd = *cmd] {
        // 實例化一個 對象
        types::types_t types;
        // 初始化 對象 屬性
        {
            types.int16_ = -16;                 //基本類型
            types.ok_ = true;                   // 布爾
            types.str_ = "cerberus is an idea"; // 字符串
            {
                // 另外一個 對象
                BINARY_CPP_SHARED_PTR(zoo::animal::cat_t)
                cat = BINARY_CPP_MAKE_SHARED(zoo::animal::cat_t);
                types.cat_ = cat;
            }
            types.pos_ = zoo::animal::pos_t::cage; // 枚舉
        }
        // 初始化 數組屬性
        {
            // 基本類型
            for (int i = 0; i < 16; i++)
            {
                types.int16s_.push_back((int16_t)(-16 + i));
            }
            // 布爾
            for (int i = 0; i < 65; i++)
            {
                types.int16s_.push_back(i % 2);
            }
            // 字符串
            types.strs_.push_back("str0");
            types.strs_.push_back("");
            types.strs_.push_back("str1");

            // 其它對象
            types.cats_.push_back(BINARY_CPP_MAKE_SHARED(zoo::animal::cat_t));
            types.cats_.push_back(NULL);

            // 枚舉
            types.poss_.push_back(zoo::animal::pos_t::corridor_p0);
            types.poss_.push_back(zoo::animal::pos_t::corridor_p1);
        }

        // 返回 默認的 編碼環境
        binarycpp::context_t ctx = binarycpp::background();

        // 編碼數據
        int marshal_size = types.marshal_size(ctx);
        boost::shared_array<uint8_t> data(new uint8_t[marshal_size]);
        if (!data)
        {
            throw CLI::RuntimeError(-1);
        }
        int n = types.marshal(ctx, data.get(), marshal_size, 0);
        if (n != marshal_size)
        {
            throw CLI::RuntimeError(n);
        }

        // 釋放 對象資源 析構函數會自動釋放
        types.reset();

        // 解碼數據
        types.unmarshal(ctx, data.get(), marshal_size);

        throw CLI::Success();
    });
}
```