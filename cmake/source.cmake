list(APPEND target_sources
    src/main.cpp

    src/cmd/test.cpp
    src/cmd/byte_order.cpp
    src/cmd/types.cpp
    src/cmd/example.cpp
    src/cmd/io.cpp

    src/cpp/binary_cpp.cpp
    
    src/protocol/types.cpp
    src/protocol/zoo/animal.cpp
)
file(GLOB files 
    "${CMAKE_SOURCE_DIR}/src/cmd/test/*.cpp" 
)
list(APPEND target_sources ${files})