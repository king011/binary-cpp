find_program(GRPC_CPP_PLUGIN grpc_cpp_plugin) # Get full path to plugin

include(FindPackageHandleStandardArgs)

set(GRPC_FOUND FALSE)
set(GRPC_INCLUDE_DIRS)
set(GRPC_LIBRARIES)

# 查找 頭文件
find_path(GRPC_INCLUDE_DIRS NAMES grpc/grpc.h)
if(NOT GRPC_INCLUDE_DIRS)
    if(GRPC_FIND_REQUIRED)
        message(FATAL_ERROR "GRPC : Could not find grpc/grpc.h")
    else()
        message(WARNING "GRPC : Could not find grpc/grpc.h")
    endif()
    return()
endif()


# 查找 庫文件
find_library(GRPC_LIBRARIE_GRPC NAMES grpc)
if(NOT GRPC_LIBRARIE_GRPC)
    if(GRPC_FIND_REQUIRED)
        message(FATAL_ERROR "GRPC : Could not find lib grpc")
    else()
        message(WARNING "GRPC : Could not find lib grpc")
    endif()
    return()
endif()
find_library(GRPC_LIBRARIE_GRPCPP NAMES grpc++)
if(NOT GRPC_LIBRARIE_GRPCPP)
    if(GRPC_FIND_REQUIRED)
        message(FATAL_ERROR "GRPC : Could not find lib grpc++")
    else()
        message(WARNING "GRPC : Could not find lib grpc++")
    endif()
    return()
endif()
find_library(GRPC_LIBRARIE_GRP NAMES gpr)
if(NOT GRPC_LIBRARIE_GRP)
    if(GRPC_FIND_REQUIRED)
        message(FATAL_ERROR "GRPC : Could not find lib gpr")
    else()
        message(WARNING "GRPC : Could not find lib gpr")
    endif()
    return()
endif()
list(APPEND GRPC_LIBRARIES
    "${GRPC_LIBRARIE_GRPC}"
    "${GRPC_LIBRARIE_GRPCPP}"
    "${GRPC_LIBRARIE_GRP}"
)
list(REMOVE_DUPLICATES GRPC_INCLUDE_DIRS)
list(REMOVE_DUPLICATES GRPC_LIBRARIES)

message(STATUS "Found GRPC: ${GRPC_LIBRARIES}; plugin - ${GRPC_CPP_PLUGIN}")

# 設置 庫 信息
find_package_handle_standard_args(GRPC
		DEFAULT_MSG
		GRPC_INCLUDE_DIRS
        GRPC_LIBRARIES
)
