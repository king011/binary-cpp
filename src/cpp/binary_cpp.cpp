#include "binary_cpp.h"
namespace binarycpp
{
bool bit_get(uint8_t val, std::size_t i)
{
    switch (i)
    {
    case 0:
        val &= 1;
        break;
    case 1:
        val &= 2;
        break;
    case 2:
        val &= 4;
        break;
    case 3:
        val &= 8;
        break;
    case 4:
        val &= 16;
        break;
    case 5:
        val &= 32;
        break;
    case 6:
        val &= 64;
        break;
    case 7:
        val &= 128;
        break;
    default:
        BINARY_CPP_THROW(BINARY_CPP_ERROR_RANGE, "bit_get error range")
    }
    return val != 0;
}
void bit_set(uint8_t *val, int i, bool ok)
{
    switch (i)
    {
    case 0:
        if (ok)
        {
            *val |= 1;
        }
        else
        {
            *val &= ~1;
        }
        break;
    case 1:
        if (ok)
        {
            *val |= 2;
        }
        else
        {
            *val &= ~2;
        }
        break;
    case 2:
        if (ok)
        {
            *val |= 4;
        }
        else
        {
            *val &= ~4;
        }
        break;
    case 3:
        if (ok)
        {
            *val |= 8;
        }
        else
        {
            *val &= ~8;
        }
        break;
    case 4:
        if (ok)
        {
            *val |= 16;
        }
        else
        {
            *val &= ~16;
        }
        break;
    case 5:
        if (ok)
        {
            *val |= 32;
        }
        else
        {
            *val &= ~32;
        }
        break;
    case 6:
        if (ok)
        {
            *val |= 64;
        }
        else
        {
            *val &= ~64;
        }
        break;
    case 7:
        if (ok)
        {
            *val |= 128;
        }
        else
        {
            *val &= ~128;
        }
        break;
    default:
        BINARY_CPP_THROW(BINARY_CPP_ERROR_RANGE, "bit_set error range")
    }
}

uint16_t internal_uint16(const uint8_t *data)
{
    return *((uint16_t *)data);
}
uint16_t internal_uint16_different(const uint8_t *data)
{
    return (uint16_t)data[0] << 8 |
           (uint16_t)data[1];
}
void internal_put_uint16(uint8_t *data, uint16_t val)
{
    *((uint16_t *)data) = val;
}
void internal_put_uint16_different(uint8_t *data, uint16_t val)
{
    data[0] = (uint8_t)(val >> 8);
    data[1] = (uint8_t)(val & 0xff);
}
uint32_t internal_uint32(const uint8_t *data)
{
    return *((uint32_t *)data);
}
uint32_t internal_uint32_different(const uint8_t *data)
{
    return (uint32_t)data[0] << 24 |
           (uint32_t)data[1] << 16 |
           (uint32_t)data[2] << 8 |
           (uint32_t)data[3];
}
void internal_put_uint32(uint8_t *data, uint32_t val)
{
    *((uint32_t *)data) = val;
}
void internal_put_uint32_different(uint8_t *data, uint32_t val)
{
    data[0] = (uint8_t)(val >> 24);
    data[1] = (uint8_t)(val >> 16);
    data[2] = (uint8_t)(val >> 8);
    data[3] = (uint8_t)(val);
}
uint64_t internal_uint64(const uint8_t *data)
{
    return *((uint64_t *)data);
}
uint64_t internal_uint64_different(const uint8_t *data)
{
    return (uint64_t)data[0] << 56 |
           (uint64_t)data[1] << 48 |
           (uint64_t)data[2] << 40 |
           (uint64_t)data[3] << 32 |
           (uint64_t)data[4] << 24 |
           (uint64_t)data[5] << 16 |
           (uint64_t)data[6] << 8 |
           (uint64_t)data[7];
}

void internal_put_uint64(uint8_t *data, uint64_t val)
{
    *((uint64_t *)data) = val;
}
void internal_put_uint64_different(uint8_t *data, uint64_t val)
{
    data[0] = (uint8_t)(val >> 56);
    data[1] = (uint8_t)(val >> 48);
    data[2] = (uint8_t)(val >> 40);
    data[3] = (uint8_t)(val >> 32);
    data[4] = (uint8_t)(val >> 24);
    data[5] = (uint8_t)(val >> 16);
    data[6] = (uint8_t)(val >> 8);
    data[7] = (uint8_t)(val);
}
void byte_order(byte_order_t &order, bool different)
{
    if (different)
    {
        order.uint16 = internal_uint16_different;
        order.uint32 = internal_uint32_different;
        order.uint64 = internal_uint64_different;
        order.put_uint16 = internal_put_uint16_different;
        order.put_uint32 = internal_put_uint32_different;
        order.put_uint64 = internal_put_uint64_different;
    }
    else
    {
        order.uint16 = internal_uint16;
        order.uint32 = internal_uint32;
        order.uint64 = internal_uint64;
        order.put_uint16 = internal_put_uint16;
        order.put_uint32 = internal_put_uint32;
        order.put_uint64 = internal_put_uint64;
    }
}

void little_endian(byte_order_t &order)
{
    if (0x100 >> 8)
    {
        byte_order(order, false);
    }
    else
    {
        byte_order(order, true);
    }
}
void big_endian(byte_order_t &order)
{
    if (0x100 >> 8)
    {
        byte_order(order, true);
    }
    else
    {
        byte_order(order, false);
    }
}
context_t background()
{
    context_t ctx;
    ctx.max_buffer = BINARY_CPP_DEFAULT_MAX_BUFFER;
    little_endian(ctx.byte_order);
    return ctx;
}
int marshal_size_add(const context_t &ctx, int use, int add)
{
    if (add > BINARY_CPP_MAX_UINT16)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MAX_BUFFER, "add > uint16")
    }
    else if (add > ctx.max_buffer)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "add > max buffer")
    }

    use += add;
    if (use > BINARY_CPP_MAX_UINT16)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MAX_BUFFER, "use > uint16")
    }
    else if (use > ctx.max_buffer)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "use > max buffer")
    }

    return use;
}
int marshal_size(const context_t &ctx, int use, const bool v)
{
    if (v)
    {
        use = marshal_size_add(ctx, use, 2 + 1);
    }
    return use;
}
int marshal_size(const context_t &ctx, int use, const std::string &v)
{
    if (!v.empty())
    {
        use = marshal_size_add(ctx, use, 2 + 2 + v.size());
    }
    return use;
}
int marshal_size(const context_t &ctx, int use, const std::vector<bool> &v)
{
    if (!v.empty())
    {
        use = marshal_size_add(ctx, use, 2 + 2 + (v.size() + 7) / 8);
    }
    return use;
}
int marshal_size(const context_t &ctx, int use, const std::vector<std::string> &v)
{
    if (!v.empty())
    {
        use = marshal_size_add(ctx, use, 2 + 2);

        for (std::size_t i = 0; i < v.size(); i++)
        {
            use = marshal_size_add(ctx, use, 2 + v[i].size());
        }
    }
    return use;
}
int marshal_add(const context_t &ctx, int use, int add, int capacity)
{
    if (add > BINARY_CPP_MAX_UINT16)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MAX_BUFFER, "add > uint16")
    }
    else if (add > ctx.max_buffer)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "add > max buffer")
    }
    else if (add > capacity)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "add > capacity")
    }

    use += add;
    if (use > BINARY_CPP_MAX_UINT16)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MAX_BUFFER, "use > uint16")
    }
    else if (use > ctx.max_buffer)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "use > max buffer")
    }
    else if (use > capacity)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "use > capacity")
    }
    return use;
}
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const bool v)
{
    if (v)
    {
        std::uint8_t *start = buffer + use;
        use = marshal_add(ctx, use, 2 + 1, capacity);

        BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_8)
        start[2] = 1;
    }
    return use;
}
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const std::string &v)
{
    if (!v.empty())
    {
        std::uint8_t *start = buffer + use;
        use = marshal_add(ctx, use, 4 + v.size(), capacity);

        BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH)
        ctx.byte_order.put_uint16(start + 2, (uint16_t)(v.size()));
        memcpy(start + 4, v.data(), v.size());
    }
    return use;
}
int marshal_type(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const base_t *p)
{
    if (p)
    {
        std::uint8_t *start = buffer + use;
        use = marshal_add(ctx, use, 4, capacity);
        int pos = use;
        use = p->marshal(ctx, buffer, capacity, use);

        BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH);
        ctx.byte_order.put_uint16(start + 2, use - pos);
    }
    return use;
}
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, std::uint16_t id, const std::vector<bool> &v)
{
    if (!v.empty())
    {
        if (v.size() > 0xFFFF)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_MESSAGE_TOO_LONG, "message too long")
        }
        std::uint8_t *start = buffer + use;
        std::size_t size = (v.size() + 7) / 8;
        use = marshal_add(ctx, use, 4 + size, capacity);

        BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_BITS)
        ctx.byte_order.put_uint16(start + 2, (uint16_t)(v.size()));
        start += 4;
        memset(start, 0, size);
        std::size_t index;
        for (std::size_t i = 0; i < v.size(); i++)
        {
            if (v[i])
            {
                index = i / 8;
                bit_set(start + i / 8, i % 8, true);
            }
        }
    }
    return use;
}
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, std::uint16_t id, const std::vector<std::string> &v)
{
    if (!v.empty())
    {
        uint8_t *start = buffer + use;
        uint8_t *header = start;
        use = marshal_add(ctx, use, 4, capacity);
        start += 4;
        std::size_t pos = use;
        for (std::size_t i = 0; i < v.size(); i++)
        {
            const std::string &str = v[i];
            if (str.empty())
            {
                use = marshal_add(ctx, use, 2, capacity);
                ctx.byte_order.put_uint16(start, 0);
                start += 2;
            }
            else
            {
                use = marshal_add(ctx, use, 2 + str.size(), capacity);
                ctx.byte_order.put_uint16(start, (uint16_t)(str.size()));
                start += 2;
                memcpy(start, str.data(), str.size());
                start += str.size();
            }
        }

        BINARY_CPP_PUT_TAG(ctx, header, id, BINARY_CPP_WRITE_LENGTH);
        ctx.byte_order.put_uint16(header + 2, (uint16_t)(use - pos));
    }
    return use;
}
int unmarshal_field(const context_t &ctx, const std::uint8_t *buffer, int n, field_t &field)
{
    if (n < 2)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD, "unmarshal field, n < 2");
    }
    field.id = ctx.byte_order.uint16(buffer);
    int size;
    switch (BINARY_CPP_GET_WT_FROM_TAG(field.id))
    {
    case BINARY_CPP_WRITE_8:
        field.length = 1;
        field.data = buffer + 2;
        size = 2 + field.length;
        if (n < size)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < field size")
        }
        break;
    case BINARY_CPP_WRITE_16:
        field.length = 2;
        field.data = buffer + 2;
        size = 2 + field.length;
        if (n < size)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < field size")
        }
        break;
    case BINARY_CPP_WRITE_32:
        field.length = 4;
        field.data = buffer + 2;
        size = 2 + field.length;
        if (n < size)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < field size")
        }
        break;
    case BINARY_CPP_WRITE_64:
        field.length = 8;
        field.data = buffer + 2;
        size = 2 + field.length;
        if (n < size)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < field size")
        }
        break;
    case BINARY_CPP_WRITE_LENGTH:
        if (n < 4)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD, "unmarshal repeat field, n < 4 ");
        }
        field.length = ctx.byte_order.uint16(buffer + 2);
        field.data = buffer + 4;
        size = 4 + field.length;
        if (n < size)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < field size")
        }
        break;
    case BINARY_CPP_WRITE_BITS:
        if (n < 4)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD, "unmarshal bits field, n < 4 ");
        }
        field.length = ctx.byte_order.uint16(buffer + 2);
        field.data = buffer + 4;
        size = 4 + (field.length + 7) / 8;
        if (n < size)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < field size")
        }
        break;
    default:
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_UNKNOW_WRITE_TYPE, "unknow write type")
    }
    BINARY_CPP_SET_TAG_TO_ID(field.id);
    return size;
}
void unmarshal_fields(const context_t &ctx, const std::uint8_t *buffer, int n, field_t *fields, int count)
{
    int i = 0;
    int size;
    field_t field;
    while (i < count && n > 0)
    {
        size = unmarshal_field(ctx, buffer, n, field);
        buffer += size;
        n -= size;
        if (n < 0)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal field, n < 0")
        }

        if (field.id < fields[i].id)
        {
            continue;
        }
        for (; i < count; i++)
        {
            if (field.id == fields[i].id)
            {
                fields[i].length = field.length;
                fields[i].data = field.data;
                i++;
                break;
            }
            else if (field.id < fields[i].id)
            {
                break;
            }
        }
    }
}
void unmarshal(const context_t &ctx, const field_t &field, bool &output)
{
    if (field.length != 1)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal bool, n != 1")
    }
    if (field.data[0])
    {
        output = true;
    }
    else
    {
        output = false;
    }
}
void unmarshal(const context_t &ctx, const field_t &field, std::string &output)
{
    if (field.length < 1)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal string, n < 1")
    }
    else
    {
        output = std::string((const char *)field.data, field.length);
    }
}
void unmarshal(const context_t &ctx, const field_t &field, std::vector<bool> &output)
{
    if (field.length < 1)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal bits, n < 1")
    }
    else
    {
        std::vector<bool> data(field.length);
        for (int i = 0; i < field.length; i++)
        {
            data[i] = bit_get(field.data[i / 8], i % 8);
        }
        output.swap(data);
    }
}
void unmarshal_count(const context_t &ctx, const std::uint8_t *buffer, int size, std::vector<std::pair<const std::uint8_t *, int>> &arrs)
{
    int pos;
    int second;
    while (1)
    {
        if (size < 2)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal count,size < 2")
        }
        second = (int)ctx.byte_order.uint16(buffer);
        pos = 2 + second;
        buffer += pos;
        size -= pos;
        if (size < 0)
        {
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal count,size < 2 + size")
        }
        arrs.push_back(std::make_pair(buffer - second, second));
        if (size == 0)
        {
            break;
        }
    }
}

void unmarshal(const context_t &ctx, const field_t &field, std::vector<std::string> &output)
{
    if (field.length < 1)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal strings, n < 1")
    }
    else
    {
        std::vector<std::pair<const std::uint8_t *, int>> arrs;
        unmarshal_count(ctx, field.data, field.length, arrs);
        std::vector<std::string> strs(arrs.size());
        int count = arrs.size();
        for (int i = 0; i < count; i++)
        {
            if (arrs[i].second)
            {
                strs[i] = std::string((const char *)(arrs[i].first), arrs[i].second);
            }
        }
        output.swap(strs);
    }
}
std::int16_t unmarshal_enum(const context_t &ctx, const field_t &field)
{
    std::int16_t v;
    if (field.length != 2)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal enum, n not match")
    }
    else
    {
        v = (std::int16_t)ctx.byte_order.uint16(field.data);
    }
    return v;
}
}; // namespace binarycpp