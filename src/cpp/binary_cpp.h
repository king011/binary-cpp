#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <cstring>

#ifdef BINARY_CPP_USE_CC11_SHARED
#define BINARY_CPP_SHARED_PTR(t) std::shared_ptr<t>
#define BINARY_CPP_MAKE_SHARED(t) std::make_shared<t>()
#else
#include <boost/smart_ptr.hpp>
#define BINARY_CPP_SHARED_PTR(t) boost::shared_ptr<t>
#define BINARY_CPP_MAKE_SHARED(t) boost::make_shared<t>()
#endif

#ifndef BINARY_CPP_DEFAULT_MAX_BUFFER
#define BINARY_CPP_DEFAULT_MAX_BUFFER (1024 * 50)
#endif

#define BINARY_CPP_PUT_TAG(ctx, data, id, val) ctx.byte_order.put_uint16(data, id | (val << 13));
#define BINARY_CPP_GET_WT_FROM_TAG(tag) (tag >> 13)
#define BINARY_CPP_SET_TAG_TO_ID(tag) (tag &= 0x1FFF)

//  最大 id
#define BINARY_CPP_MAX_FIELD_ID 0x1FFF
#define BINARY_CPP_MAX_UINT16 0xFFFF
// byte bool
#define BINARY_CPP_WRITE_8 1
// int16 uint16 enum
#define BINARY_CPP_WRITE_16 2
// float32 int32 uint32
#define BINARY_CPP_WRITE_32 3
// float64 int64 uint64
#define BINARY_CPP_WRITE_64 4
// bytes strings type repeat
#define BINARY_CPP_WRITE_LENGTH 5
// bit repeat
#define BINARY_CPP_WRITE_BITS 6

#ifndef BINARY_CPP_FLOAT32_T
#define BINARY_CPP_FLOAT32_T float
#endif

#ifndef BINARY_CPP_FLOAT64_T
#define BINARY_CPP_FLOAT64_T double
#endif

// 成功
#define BINARY_CPP_OK 0
// 未知錯誤
#define BINARY_CPP_ERROR_UNKNOW -1
// 申請內存失敗
#define BINARY_CPP_ERROR_MALLOC -10
// 索引越界
#define BINARY_CPP_ERROR_RANGE -11

// 編碼長度 超過 uint16 限定
#define BINARY_CPP_ERROR_MAX_BUFFER -20
// 消息超過最大長度
#define BINARY_CPP_ERROR_MESSAGE_TOO_LONG -100

// 無法解開 field
#define BINARY_CPP_ERROR_UNMARSHAL_FIELD -300

// 未知 field 寫入類型
#define BINARY_CPP_ERROR_UNMARSHAL_FIELD_UNKNOW_WRITE_TYPE -301

// field 長度不匹配
#define BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH -302

// 無效的 write 類型
#define BINARY_CPP_ERROR_INVALID_WRITE_TYPE -400

#define BINARY_CPP_THROW(code, msg) throw binarycpp::exception_t(code, std::string(msg));
#define BINARY_CPP_CATCH                                    \
    catch (const binarycpp::exception_t &e)                 \
    {                                                       \
        throw e;                                            \
    }                                                       \
    catch (const std::bad_alloc &e)                         \
    {                                                       \
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MALLOC, e.what()) \
    }                                                       \
    catch (const std::exception &e)                         \
    {                                                       \
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNKNOW, e.what()) \
    }
#define BINARY_CPP_CATCH_RESET                              \
    catch (const binarycpp::exception_t &e)                 \
    {                                                       \
        reset();                                            \
        throw e;                                            \
    }                                                       \
    catch (const std::bad_alloc &e)                         \
    {                                                       \
        reset();                                            \
        BINARY_CPP_THROW(BINARY_CPP_ERROR_MALLOC, e.what()) \
    }                                                       \
    catch (const std::exception &e)                         \
    {                                                       \
        reset();                                            \
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNKNOW, e.what()) \
    }

namespace binarycpp
{
typedef uint16_t (*uint16_f)(const uint8_t *data);
typedef uint32_t (*uint32_f)(const uint8_t *data);
typedef uint64_t (*uint64_f)(const uint8_t *data);
typedef void (*put_uint16_f)(uint8_t *data, uint16_t val);
typedef void (*put_uint32_f)(uint8_t *data, uint32_t val);
typedef void (*put_uint64_f)(uint8_t *data, uint64_t val);
/**
   * 字節編碼 序列
   */
typedef struct
{
    uint16_f uint16;
    uint32_f uint32;
    uint64_f uint64;
    put_uint16_f put_uint16;
    put_uint32_f put_uint32;
    put_uint64_f put_uint64;
} byte_order_t, *byte_order_pt;
/**
 * 初始化 小端序列
 */
void little_endian(byte_order_t &order);
/**
 * 初始化 大端序列
 */
void big_endian(byte_order_t &order);

/**
 * 編碼環境定義
 */
typedef struct
{
    /**
     * 支持的 最大編碼 長度
     */
    int max_buffer;

    /**
     * 字節編碼 序列
     */
    byte_order_t byte_order;
} context_t, *context_pt;
/**
 * 返回默認的 編碼環境
 */
context_t background();

template <typename T>
void swap(T &v0, T &v1)
{
    T v = v0;
    v0 = v1;
    v1 = v;
}

/**
 * 異常定義
 */
class exception_t : public std::exception
{
private:
    int code_;
    std::string msg_;

public:
    exception_t(int code, const std::string msg) : code_(code), msg_(msg) {}
    virtual const char *what() const noexcept
    {
        return msg_.c_str();
    }
    inline int code()
    {
        return code_;
    }
};

/**
 * 自動 序列化 類型接口
 */
class base_t
{
private:
    base_t(const base_t &);
    base_t *operator=(const base_t &);

public:
    base_t() {}
    virtual ~base_t() {}
    /**
     * 返回 序列化後 數據長度
     * 
     * \exception class exception_t
     */
    virtual int marshal_size(const context_t &ctx, int use) const = 0;
    /**
     * 將內存結構 序列化 到二進制
     * \param buffer 二進制數據 緩衝區
     * \param size 緩衝區 長度
    * \return 編碼後的 數據長度
    * 
    * \exception class exception_t
    */
    virtual int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use) const = 0;
    /**
     * 由二進制數據 反序到 內存結構
     * 
     * \param input 二進制數據 緩衝區
     * \param size 二進制數據 長度
     * 
     * \exception class exception_t
     */
    virtual void unmarshal(const context_t &ctx, const std::uint8_t *input, std::size_t size) = 0;
    /**
     * 將屬性 設置爲 初始值
     * 
     */
    virtual void reset() = 0;
};

template <typename T>
void copy_from_types(std::vector<BINARY_CPP_SHARED_PTR(T)> &dst, const std::vector<BINARY_CPP_SHARED_PTR(T)> &src)
{
    if (src.empty())
    {
        dst.clear();
    }
    else
    {
        std::vector<BINARY_CPP_SHARED_PTR(T)> output;
        try
        {
            output = std::vector<BINARY_CPP_SHARED_PTR(T)>(src.size(), BINARY_CPP_SHARED_PTR(T)());
            BINARY_CPP_SHARED_PTR(T)
            p;
            std::size_t count = src.size();
            for (std::size_t i = 0; i < count; i++)
            {
                if (src[i])
                {
                    output[i] = src[i]->clone();
                }
            }
            output.swap(dst);
        }
        BINARY_CPP_CATCH
    }
}

int marshal_size_add(const context_t &ctx, int use, int add);
template <typename T>
int marshal_size(const context_t &ctx, int use, const T v)
{
    if (v)
    {
        use = marshal_size_add(ctx, use, 2 + sizeof(T));
    }
    return use;
}
int marshal_size(const context_t &ctx, int use, const bool v);
int marshal_size(const context_t &ctx, int use, const std::string &v);

template <typename T>
int marshal_size(const context_t &ctx, int use, const std::vector<T> &v)
{
    if (!v.empty())
    {
        use = marshal_size_add(ctx, use, 4 + v.size() * sizeof(T));
    }
    return use;
}
int marshal_size(const context_t &ctx, int use, const std::vector<bool> &v);
int marshal_size(const context_t &ctx, int use, const std::vector<std::string> &v);
template <typename T>
int marshal_types_size(const context_t &ctx, int use, const std::vector<BINARY_CPP_SHARED_PTR(T)> &arrs)
{
    if (!arrs.empty())
    {
        use = binarycpp::marshal_size_add(ctx, use, 4);
        for (std::size_t i = 0; i < arrs.size(); i++)
        {
            use = binarycpp::marshal_size_add(ctx, use, 2);
            if (arrs[i])
            {
                use = arrs[i]->marshal_size(ctx, use);
            }
        }
    }
    return use;
}

int marshal_add(const context_t &ctx, int use, int add, int capacity);
template <typename T>
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const T v)
{
    if (v)
    {
        std::uint8_t *start = buffer + use;

        int valSize = sizeof(T);
        use = marshal_add(ctx, use, 2 + valSize, capacity);

        switch (valSize)
        {
        case 1:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_8)
            start[2] = *(std::uint8_t *)(&v);
            break;
        case 2:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_16)
            ctx.byte_order.put_uint16(start + 2, *(std::uint16_t *)(&v));
            break;
        case 4:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_32)
            ctx.byte_order.put_uint32(start + 2, *(std::uint32_t *)(&v));
            break;
        case 8:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_64)
            ctx.byte_order.put_uint64(start + 2, *(std::uint64_t *)(&v));
            break;
        default:
            BINARY_CPP_THROW(BINARY_CPP_ERROR_INVALID_WRITE_TYPE, "marshal, invalid write type")
            break;
        }
    }
    return use;
}
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const bool v);
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const std::string &v);
int marshal_type(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const base_t *p);

template <typename T>
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const std::vector<T> &v)
{
    if (!v.empty())
    {
        std::uint8_t *start = buffer + use;
        std::size_t valSize = sizeof(T);
        std::size_t size = v.size() * valSize;
        use = marshal_add(ctx, use, 4 + size, capacity);
        switch (valSize)
        {
        case 1:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH)
            ctx.byte_order.put_uint16(start + 2, (std::uint16_t)(size));
            memcpy(start + 4, v.data(), size);
            return use;
        case 2:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH)
            ctx.byte_order.put_uint16(start + 2, (std::uint16_t)(size));
            break;
        case 4:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH)
            ctx.byte_order.put_uint16(start + 2, (std::uint16_t)(size));
            break;
        case 8:
            BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH)
            ctx.byte_order.put_uint16(start + 2, (std::uint16_t)(size));
            break;
        default:
            BINARY_CPP_THROW(BINARY_CPP_ERROR_INVALID_WRITE_TYPE, "marshal repeat,invalid write type")
        }
        start += 4;
        for (std::size_t i = 0; i < v.size(); i++)
        {
            switch (valSize)
            {
            case 2:
                ctx.byte_order.put_uint16(start, *(uint16_t *)(&v[i]));
                break;
            case 4:
                ctx.byte_order.put_uint32(start, *(uint32_t *)(&v[i]));
                break;
            case 8:
                ctx.byte_order.put_uint64(start, *(uint64_t *)(&v[i]));
                break;
            }
            start += valSize;
        }
    }
    return use;
}
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, std::uint16_t id, const std::vector<bool> &v);
int marshal(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, std::uint16_t id, const std::vector<std::string> &v);
template <typename T>
int marshal_enums(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const std::vector<T> &v)
{
    if (!v.empty())
    {
        std::uint8_t *start = buffer + use;
        std::size_t size = v.size() * 2;
        use = marshal_add(ctx, use, 4 + size, capacity);

        BINARY_CPP_PUT_TAG(ctx, start, id, BINARY_CPP_WRITE_LENGTH)
        ctx.byte_order.put_uint16(start + 2, (std::uint16_t)(size));
        start += 4;

        for (std::size_t i = 0; i < v.size(); i++)
        {
            ctx.byte_order.put_uint16(start, *(uint16_t *)(&v[i]));
            start += 2;
        }
    }
    return use;
}
template <typename T>
int marshal_types(const context_t &ctx, std::uint8_t *buffer, int capacity, int use, uint16_t id, const std::vector<BINARY_CPP_SHARED_PTR(T)> &arrs)
{
    if (!arrs.empty())
    {
        uint8_t *header = buffer + use;
        use = marshal_add(ctx, use, 4, capacity);
        std::size_t pos = use;
        for (std::size_t i = 0; i < arrs.size(); i++)
        {
            const BINARY_CPP_SHARED_PTR(T) &v = arrs[i];
            if (v)
            {
                uint8_t *start = buffer + use;
                use = marshal_add(ctx, use, 2, capacity);
                int class_pos = use;
                use = v->marshal(ctx, buffer, capacity, use);
                ctx.byte_order.put_uint16(start, (uint16_t)(use - class_pos));
            }
            else
            {
                uint8_t *start = buffer + use;
                use = marshal_add(ctx, use, 2, capacity);
                ctx.byte_order.put_uint16(start, 0);
            }
        }

        BINARY_CPP_PUT_TAG(ctx, header, id, BINARY_CPP_WRITE_LENGTH);
        ctx.byte_order.put_uint16(header + 2, (uint16_t)(use - pos));
    }
    return use;
}
/**
 * 二進制 字段信息
 */
struct field_t
{
    uint16_t id;
    const uint8_t *data;
    int length;
};
void unmarshal_count(const context_t &ctx, const std::uint8_t *buffer, int size, std::vector<std::pair<const std::uint8_t *, int>> &arrs);
int unmarshal_field(const context_t &ctx, const std::uint8_t *buffer, int n, field_t &field);
void unmarshal_fields(const context_t &ctx, const std::uint8_t *buffer, int n, field_t *fields, int count);

template <typename T>
void unmarshal(const context_t &ctx, const field_t &field, T &output)
{
    int valSize = sizeof(T);
    if (field.length != valSize)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal , val size not match")
    }
    else if (valSize == 1)
    {
        output = *((T *)field.data);
    }
    else
    {
        switch (valSize)
        {
        case 2:
        {
            std::uint16_t v = ctx.byte_order.uint16(field.data);
            output = *((T *)(&v));
        }
        break;
        case 4:
        {
            std::uint32_t v = ctx.byte_order.uint32(field.data);
            output = *((T *)(&v));
        }
        break;
        case 8:
        {
            std::uint64_t v = ctx.byte_order.uint64(field.data);
            output = *((T *)(&v));
        }
        break;
        default:
            BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal , val size invalid")
        }
    }
}
void unmarshal(const context_t &ctx, const field_t &field, bool &output);
void unmarshal(const context_t &ctx, const field_t &field, std::string &output);
std::int16_t unmarshal_enum(const context_t &ctx, const field_t &field);
template <typename T>
void unmarshal_type(const context_t &ctx, const field_t &field, BINARY_CPP_SHARED_PTR(T) & output)
{

    BINARY_CPP_SHARED_PTR(T)
    p = BINARY_CPP_MAKE_SHARED(T);
    p->unmarshal(ctx, field.data, field.length);
    output = p;
}

template <typename T>
void unmarshal(const context_t &ctx, const field_t &field, std::vector<T> &output)
{
    std::size_t valSize = sizeof(T);
    if (field.length < 1)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal repeat , val size < 1")
    }
    else if (field.length % valSize)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal repeat , val size % fault")
    }

    int count = field.length / valSize;
    std::vector<T> data(count);
    if (valSize == 1)
    {
        memcpy(data.data(), field.data, field.length);
    }
    else
    {
        for (int i = 0; i < count; i++)
        {
            switch (valSize)
            {
            case 2:
            {
                std::uint16_t v = ctx.byte_order.uint16(field.data + i * valSize);
                data[i] = *((T *)(&v));
            }
            break;

            case 4:
            {
                std::uint32_t v = ctx.byte_order.uint32(field.data + i * valSize);
                data[i] = *((T *)(&v));
            }
            break;

            case 8:
            {
                std::uint64_t v = ctx.byte_order.uint64(field.data + i * valSize);
                data[i] = *((T *)(&v));
            }
            break;

            default:
                BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal repeat ,val size invalid")
            }
        }
    }
    output.swap(data);
}
void unmarshal(const context_t &ctx, const field_t &field, std::vector<bool> &output);
void unmarshal(const context_t &ctx, const field_t &field, std::vector<std::string> &output);
template <typename T>
void unmarshal_enums(const context_t &ctx, const field_t &field, std::vector<T> &output)
{
    if (field.length < 1)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal enums, n < 1")
    }
    else if (field.length % 2)
    {
        BINARY_CPP_THROW(BINARY_CPP_ERROR_UNMARSHAL_FIELD_LENGTH, "unmarshal enums, length fault")
    }

    std::size_t count = field.length / 2;
    std::vector<T> data(count);

    for (std::size_t i = 0; i < count; i++)
    {
        data[i] = (T)ctx.byte_order.uint16(field.data + i * 2);
    }
    output.swap(data);
}
template <typename T>
void unmarshal_types(const context_t &ctx, const field_t &field, std::vector<BINARY_CPP_SHARED_PTR(T)> &output)
{
    std::vector<std::pair<const std::uint8_t *, int>> arrs;
    unmarshal_count(ctx, field.data, field.length, arrs);
    if (arrs.empty())
    {
        return;
    }
    int count = arrs.size();
    std::vector<BINARY_CPP_SHARED_PTR(T)> items(count);

    BINARY_CPP_SHARED_PTR(T)
    p;
    for (int i = 0; i < count; i++)
    {
        if (arrs[i].second)
        {
            p = BINARY_CPP_MAKE_SHARED(T);
            p->unmarshal(ctx, arrs[i].first, arrs[i].second);
            items[i] = p;
        }
    }
    output.swap(items);
}

}; // namespace binarycpp