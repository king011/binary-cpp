#include "types.h"
namespace types
{
types_t::types_t()
{
	reset();
}
types_t::~types_t()
{
}
void types_t::reset()
{
	int16_ = 0;
	int32_ = 0;
	int64_ = 0;
	uint16_ = 0;
	uint32_ = 0;
	uint64_ = 0;
	float32_ = 0;
	float64_ = 0;
	b_ = 0;
	ok_ = false;
	str_.clear();
	cat_.reset();
	dog_.reset();
	pos_ = (zoo::animal::pos_t)(0);
	int16s_.clear();
	int32s_.clear();
	int64s_.clear();
	uint16s_.clear();
	uint32s_.clear();
	uint64s_.clear();
	float32s_.clear();
	float64s_.clear();
	bs_.clear();
	oks_.clear();
	strs_.clear();
	cats_.clear();
	dogs_.clear();
	poss_.clear();
}
void types_t::swap(types_t &other)
{
	binarycpp::swap(int16_, other.int16_);
	binarycpp::swap(int32_, other.int32_);
	binarycpp::swap(int64_, other.int64_);
	binarycpp::swap(uint16_, other.uint16_);
	binarycpp::swap(uint32_, other.uint32_);
	binarycpp::swap(uint64_, other.uint64_);
	binarycpp::swap(float32_, other.float32_);
	binarycpp::swap(float64_, other.float64_);
	binarycpp::swap(b_, other.b_);
	binarycpp::swap(ok_, other.ok_);
	binarycpp::swap(str_, other.str_);
	cat_.swap(other.cat_);
	dog_.swap(other.dog_);
	binarycpp::swap(pos_, other.pos_);
	int16s_.swap(other.int16s_);
	int32s_.swap(other.int32s_);
	int64s_.swap(other.int64s_);
	uint16s_.swap(other.uint16s_);
	uint32s_.swap(other.uint32s_);
	uint64s_.swap(other.uint64s_);
	float32s_.swap(other.float32s_);
	float64s_.swap(other.float64s_);
	bs_.swap(other.bs_);
	oks_.swap(other.oks_);
	strs_.swap(other.strs_);
	cats_.swap(other.cats_);
	dogs_.swap(other.dogs_);
	poss_.swap(other.poss_);
}
void types_t::copy_from(const types_t &other)
{
	try
	{
		int16_ = other.int16_;
		int32_ = other.int32_;
		int64_ = other.int64_;
		uint16_ = other.uint16_;
		uint32_ = other.uint32_;
		uint64_ = other.uint64_;
		float32_ = other.float32_;
		float64_ = other.float64_;
		b_ = other.b_;
		ok_ = other.ok_;
		str_ = other.str_;
		if (other.cat_)
		{
			cat_ = other.cat_->clone();
		}
		else
		{
			cat_.reset();
		}
		if (other.dog_)
		{
			dog_ = other.dog_->clone();
		}
		else
		{
			dog_.reset();
		}
		pos_ = other.pos_;
		int16s_ = other.int16s_;
		int32s_ = other.int32s_;
		int64s_ = other.int64s_;
		uint16s_ = other.uint16s_;
		uint32s_ = other.uint32s_;
		uint64s_ = other.uint64s_;
		float32s_ = other.float32s_;
		float64s_ = other.float64s_;
		bs_ = other.bs_;
		oks_ = other.oks_;
		strs_ = other.strs_;
		binarycpp::copy_from_types(cats_, other.cats_);
		binarycpp::copy_from_types(dogs_, other.dogs_);
		poss_ = other.poss_;
	}
	BINARY_CPP_CATCH_RESET
}
void types_t::copy_from(const types_t *other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
void types_t::copy_from(const BINARY_CPP_SHARED_PTR(types_t) & other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
BINARY_CPP_SHARED_PTR(types_t)
types_t::clone() const
{
	BINARY_CPP_SHARED_PTR(types_t)
	p;
	try
	{
		p = BINARY_CPP_MAKE_SHARED(types_t);
		p->copy_from(*this);
	}
	BINARY_CPP_CATCH
	return p;
}
int types_t::marshal_size(const binarycpp::context_t &ctx, int use) const
{
	use = binarycpp::marshal_size(ctx, use, int16_);
	use = binarycpp::marshal_size(ctx, use, int32_);
	use = binarycpp::marshal_size(ctx, use, int64_);
	use = binarycpp::marshal_size(ctx, use, uint16_);
	use = binarycpp::marshal_size(ctx, use, uint32_);
	use = binarycpp::marshal_size(ctx, use, uint64_);
	use = binarycpp::marshal_size(ctx, use, float32_);
	use = binarycpp::marshal_size(ctx, use, float64_);
	use = binarycpp::marshal_size(ctx, use, b_);
	use = binarycpp::marshal_size(ctx, use, ok_);
	use = binarycpp::marshal_size(ctx, use, str_);
	if (cat_)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
		use = cat_->marshal_size(ctx, use);
	}
	if (dog_)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
		use = dog_->marshal_size(ctx, use);
	}
	if (pos_)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
	}
	use = binarycpp::marshal_size(ctx, use, int16s_);
	use = binarycpp::marshal_size(ctx, use, int32s_);
	use = binarycpp::marshal_size(ctx, use, int64s_);
	use = binarycpp::marshal_size(ctx, use, uint16s_);
	use = binarycpp::marshal_size(ctx, use, uint32s_);
	use = binarycpp::marshal_size(ctx, use, uint64s_);
	use = binarycpp::marshal_size(ctx, use, float32s_);
	use = binarycpp::marshal_size(ctx, use, float64s_);
	use = binarycpp::marshal_size(ctx, use, bs_);
	use = binarycpp::marshal_size(ctx, use, oks_);
	use = binarycpp::marshal_size(ctx, use, strs_);
	if (!cats_.empty())
	{
		use = binarycpp::marshal_types_size(ctx, use, cats_);
	}
	if (!dogs_.empty())
	{
		use = binarycpp::marshal_types_size(ctx, use, dogs_);
	}
	if (!poss_.empty())
	{
        use = binarycpp::marshal_size_add(ctx, use, 4 + poss_.size() * 2);
	}
	return use;
}
int types_t::marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use) const
{
	use = binarycpp::marshal(ctx, buffer, capacity, use, 1, int16_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 2, int32_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 3, int64_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 4, uint16_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 5, uint32_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 6, uint64_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 7, float32_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 8, float64_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 9, b_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 10, ok_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 11, str_);
	if (cat_)
	{
		use = binarycpp::marshal_type(ctx, buffer, capacity, use, 12, cat_.get());
	}
	if (dog_)
	{
		use = binarycpp::marshal_type(ctx, buffer, capacity, use, 13, dog_.get());
	}
	use = binarycpp::marshal(ctx, buffer, capacity, use, 14, (std::int16_t)(pos_));
	use = binarycpp::marshal(ctx, buffer, capacity, use, 21, int16s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 22, int32s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 23, int64s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 24, uint16s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 25, uint32s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 26, uint64s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 27, float32s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 28, float64s_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 29, bs_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 30, oks_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 31, strs_);
	if (!cats_.empty())
	{
		use = binarycpp::marshal_types(ctx, buffer, capacity, use, 32, cats_);
	}
	if (!dogs_.empty())
	{
		use = binarycpp::marshal_types(ctx, buffer, capacity, use, 33, dogs_);
	}
	if (!poss_.empty())
	{
		use = binarycpp::marshal_enums(ctx, buffer, capacity, use, 34, poss_);
	}
	return use;
}
void types_t::unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n)
{
	reset();
	try
	{
		binarycpp::field_t fields[28];
		memset(fields, 0, sizeof(binarycpp::field_t) * 28);
		fields[0].id = 1;
		fields[1].id = 2;
		fields[2].id = 3;
		fields[3].id = 4;
		fields[4].id = 5;
		fields[5].id = 6;
		fields[6].id = 7;
		fields[7].id = 8;
		fields[8].id = 9;
		fields[9].id = 10;
		fields[10].id = 11;
		fields[11].id = 12;
		fields[12].id = 13;
		fields[13].id = 14;
		fields[14].id = 21;
		fields[15].id = 22;
		fields[16].id = 23;
		fields[17].id = 24;
		fields[18].id = 25;
		fields[19].id = 26;
		fields[20].id = 27;
		fields[21].id = 28;
		fields[22].id = 29;
		fields[23].id = 30;
		fields[24].id = 31;
		fields[25].id = 32;
		fields[26].id = 33;
		fields[27].id = 34;
		binarycpp::unmarshal_fields(ctx, input, n, fields, 28);
		if (fields[0].length)
		{
			binarycpp::unmarshal(ctx, fields[0], int16_);
		}
		if (fields[1].length)
		{
			binarycpp::unmarshal(ctx, fields[1], int32_);
		}
		if (fields[2].length)
		{
			binarycpp::unmarshal(ctx, fields[2], int64_);
		}
		if (fields[3].length)
		{
			binarycpp::unmarshal(ctx, fields[3], uint16_);
		}
		if (fields[4].length)
		{
			binarycpp::unmarshal(ctx, fields[4], uint32_);
		}
		if (fields[5].length)
		{
			binarycpp::unmarshal(ctx, fields[5], uint64_);
		}
		if (fields[6].length)
		{
			binarycpp::unmarshal(ctx, fields[6], float32_);
		}
		if (fields[7].length)
		{
			binarycpp::unmarshal(ctx, fields[7], float64_);
		}
		if (fields[8].length)
		{
			binarycpp::unmarshal(ctx, fields[8], b_);
		}
		if (fields[9].length)
		{
			binarycpp::unmarshal(ctx, fields[9], ok_);
		}
		if (fields[10].length)
		{
			binarycpp::unmarshal(ctx, fields[10], str_);
		}
		if (fields[11].length)
		{
			binarycpp::unmarshal_type(ctx, fields[11], cat_);
		}
		if (fields[12].length)
		{
			binarycpp::unmarshal_type(ctx, fields[12], dog_);
		}
		if (fields[13].length)
		{
			pos_ = (zoo::animal::pos_t)binarycpp::unmarshal_enum(ctx, fields[13]);
		}
		if (fields[14].length)
		{
			binarycpp::unmarshal(ctx, fields[14], int16s_);
		}
		if (fields[15].length)
		{
			binarycpp::unmarshal(ctx, fields[15], int32s_);
		}
		if (fields[16].length)
		{
			binarycpp::unmarshal(ctx, fields[16], int64s_);
		}
		if (fields[17].length)
		{
			binarycpp::unmarshal(ctx, fields[17], uint16s_);
		}
		if (fields[18].length)
		{
			binarycpp::unmarshal(ctx, fields[18], uint32s_);
		}
		if (fields[19].length)
		{
			binarycpp::unmarshal(ctx, fields[19], uint64s_);
		}
		if (fields[20].length)
		{
			binarycpp::unmarshal(ctx, fields[20], float32s_);
		}
		if (fields[21].length)
		{
			binarycpp::unmarshal(ctx, fields[21], float64s_);
		}
		if (fields[22].length)
		{
			binarycpp::unmarshal(ctx, fields[22], bs_);
		}
		if (fields[23].length)
		{
			binarycpp::unmarshal(ctx, fields[23], oks_);
		}
		if (fields[24].length)
		{
			binarycpp::unmarshal(ctx, fields[24], strs_);
		}
		if (fields[25].length)
		{
			binarycpp::unmarshal_types(ctx, fields[25], cats_);
		}
		if (fields[26].length)
		{
			binarycpp::unmarshal_types(ctx, fields[26], dogs_);
		}
		if (fields[27].length)
		{
			binarycpp::unmarshal_enums(ctx, fields[27], poss_);
		}
	}
	BINARY_CPP_CATCH_RESET
}
}; // namespace types
