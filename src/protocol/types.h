#pragma once
#include <binary_cpp.h>
#include <zoo/animal.h>
namespace types
{
class types_t;
class types_t : public binarycpp::base_t
{
public:
	types_t();
	virtual ~types_t();
	virtual int marshal_size(const binarycpp::context_t &ctx, int use = 0) const;
	virtual int marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use = 0) const;
	virtual void unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n);
	virtual void reset();
	void swap(types_t &other);
	void copy_from(const types_t &other);
	void copy_from(const types_t *other);
	void copy_from(const BINARY_CPP_SHARED_PTR(types_t) & other);
	BINARY_CPP_SHARED_PTR(types_t)
	clone() const;
	std::int16_t int16_;
	std::int32_t int32_;
	std::int64_t int64_;
	std::uint16_t uint16_;
	std::uint32_t uint32_;
	std::uint64_t uint64_;
	BINARY_CPP_FLOAT32_T float32_;
	BINARY_CPP_FLOAT64_T float64_;
	std::uint8_t b_;
	bool ok_;
	std::string str_;
	BINARY_CPP_SHARED_PTR(zoo::animal::cat_t) cat_;
	BINARY_CPP_SHARED_PTR(zoo::animal::dog_t) dog_;
	zoo::animal::pos_t pos_;
	std::vector<std::int16_t> int16s_;
	std::vector<std::int32_t> int32s_;
	std::vector<std::int64_t> int64s_;
	std::vector<std::uint16_t> uint16s_;
	std::vector<std::uint32_t> uint32s_;
	std::vector<std::uint64_t> uint64s_;
	std::vector<BINARY_CPP_FLOAT32_T> float32s_;
	std::vector<BINARY_CPP_FLOAT64_T> float64s_;
	std::vector<std::uint8_t> bs_;
	std::vector<bool> oks_;
	std::vector<std::string> strs_;
	std::vector<BINARY_CPP_SHARED_PTR(zoo::animal::cat_t)> cats_;
	std::vector<BINARY_CPP_SHARED_PTR(zoo::animal::dog_t)> dogs_;
	std::vector<zoo::animal::pos_t> poss_;
};

}; // namespace types
