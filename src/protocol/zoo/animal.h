#pragma once
#include <binary_cpp.h>
namespace zoo::animal
{
enum pos_t
{
	pool = 0,
	cage = 10,
	step = 11,
	stairs = 12,
	corridor_p0 = 13,
	corridor_p1 = 14,
};
const char *enum_pos_string(pos_t val);
pos_t enum_pos_val(const char *str);

class cat_t;
class dog_t;
class fish_t;
class cat_t : public binarycpp::base_t
{
public:
	cat_t();
	virtual ~cat_t();
	virtual int marshal_size(const binarycpp::context_t &ctx, int use = 0) const;
	virtual int marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use = 0) const;
	virtual void unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n);
	virtual void reset();
	void swap(cat_t &other);
	void copy_from(const cat_t &other);
	void copy_from(const cat_t *other);
	void copy_from(const BINARY_CPP_SHARED_PTR(cat_t) & other);
	BINARY_CPP_SHARED_PTR(cat_t)
	clone() const;
	std::string name_;
	BINARY_CPP_SHARED_PTR(zoo::animal::dog_t) dog_;
	BINARY_CPP_SHARED_PTR(zoo::animal::cat_t) cat_;
};

class dog_t : public binarycpp::base_t
{
public:
	dog_t();
	virtual ~dog_t();
	virtual int marshal_size(const binarycpp::context_t &ctx, int use = 0) const;
	virtual int marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use = 0) const;
	virtual void unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n);
	virtual void reset();
	void swap(dog_t &other);
	void copy_from(const dog_t &other);
	void copy_from(const dog_t *other);
	void copy_from(const BINARY_CPP_SHARED_PTR(dog_t) & other);
	BINARY_CPP_SHARED_PTR(dog_t)
	clone() const;
	std::int64_t id_;
	std::vector<std::string> eat_;
	BINARY_CPP_SHARED_PTR(zoo::animal::cat_t) cat_;
};

class fish_t : public binarycpp::base_t
{
public:
	fish_t();
	virtual ~fish_t();
	virtual int marshal_size(const binarycpp::context_t &ctx, int use = 0) const;
	virtual int marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use = 0) const;
	virtual void unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n);
	virtual void reset();
	void swap(fish_t &other);
	void copy_from(const fish_t &other);
	void copy_from(const fish_t *other);
	void copy_from(const BINARY_CPP_SHARED_PTR(fish_t) & other);
	BINARY_CPP_SHARED_PTR(fish_t)
	clone() const;
};

}; // namespace zoo::animal
