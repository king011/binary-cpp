#include "animal.h"
namespace zoo::animal
{
const char *enum_pos_string(pos_t val)
{
	const char *str;
	switch (val)
	{
	case 0:
		str = "pool";
		break;
	case 10:
		str = "cage";
		break;
	case 11:
		str = "step";
		break;
	case 12:
		str = "stairs";
		break;
	case 13:
		str = "corridor p0";
		break;
	case 14:
		str = "corridor p1";
		break;
	default:
		str = "unknow";
		break;
	}
	return str;
}
pos_t enum_pos_val(const char *str)
{
	uint16_t val = 0;
	if (!strcmp(str, "pool"))
	{
		val = 0;
	}
	else if (!strcmp(str, "cage"))
	{
		val = 10;
	}
	else if (!strcmp(str, "step"))
	{
		val = 11;
	}
	else if (!strcmp(str, "stairs"))
	{
		val = 12;
	}
	else if (!strcmp(str, "corridor p0"))
	{
		val = 13;
	}
	else if (!strcmp(str, "corridor p1"))
	{
		val = 14;
	}
	
	return (pos_t)(val);
}
cat_t::cat_t()
{
	reset();
}
cat_t::~cat_t()
{
}
void cat_t::reset()
{
	name_.clear();
	dog_.reset();
	cat_.reset();
}
void cat_t::swap(cat_t &other)
{
	binarycpp::swap(name_, other.name_);
	dog_.swap(other.dog_);
	cat_.swap(other.cat_);
}
void cat_t::copy_from(const cat_t &other)
{
	try
	{
		name_ = other.name_;
		if (other.dog_)
		{
			dog_ = other.dog_->clone();
		}
		else
		{
			dog_.reset();
		}
		if (other.cat_)
		{
			cat_ = other.cat_->clone();
		}
		else
		{
			cat_.reset();
		}
	}
	BINARY_CPP_CATCH_RESET
}
void cat_t::copy_from(const cat_t *other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
void cat_t::copy_from(const BINARY_CPP_SHARED_PTR(cat_t) & other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
BINARY_CPP_SHARED_PTR(cat_t)
cat_t::clone() const
{
	BINARY_CPP_SHARED_PTR(cat_t)
	p;
	try
	{
		p = BINARY_CPP_MAKE_SHARED(cat_t);
		p->copy_from(*this);
	}
	BINARY_CPP_CATCH
	return p;
}
int cat_t::marshal_size(const binarycpp::context_t &ctx, int use) const
{
	use = binarycpp::marshal_size(ctx, use, name_);
	if (dog_)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
		use = dog_->marshal_size(ctx, use);
	}
	if (cat_)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
		use = cat_->marshal_size(ctx, use);
	}
	return use;
}
int cat_t::marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use) const
{
	use = binarycpp::marshal(ctx, buffer, capacity, use, 1, name_);
	if (dog_)
	{
		use = binarycpp::marshal_type(ctx, buffer, capacity, use, 2, dog_.get());
	}
	if (cat_)
	{
		use = binarycpp::marshal_type(ctx, buffer, capacity, use, 3, cat_.get());
	}
	return use;
}
void cat_t::unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n)
{
	reset();
	try
	{
		binarycpp::field_t fields[3];
		memset(fields, 0, sizeof(binarycpp::field_t) * 3);
		fields[0].id = 1;
		fields[1].id = 2;
		fields[2].id = 3;
		binarycpp::unmarshal_fields(ctx, input, n, fields, 3);
		if (fields[0].length)
		{
			binarycpp::unmarshal(ctx, fields[0], name_);
		}
		if (fields[1].length)
		{
			binarycpp::unmarshal_type(ctx, fields[1], dog_);
		}
		if (fields[2].length)
		{
			binarycpp::unmarshal_type(ctx, fields[2], cat_);
		}
	}
	BINARY_CPP_CATCH_RESET
}
dog_t::dog_t()
{
	reset();
}
dog_t::~dog_t()
{
}
void dog_t::reset()
{
	id_ = 0;
	eat_.clear();
	cat_.reset();
}
void dog_t::swap(dog_t &other)
{
	binarycpp::swap(id_, other.id_);
	eat_.swap(other.eat_);
	cat_.swap(other.cat_);
}
void dog_t::copy_from(const dog_t &other)
{
	try
	{
		id_ = other.id_;
		eat_ = other.eat_;
		if (other.cat_)
		{
			cat_ = other.cat_->clone();
		}
		else
		{
			cat_.reset();
		}
	}
	BINARY_CPP_CATCH_RESET
}
void dog_t::copy_from(const dog_t *other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
void dog_t::copy_from(const BINARY_CPP_SHARED_PTR(dog_t) & other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
BINARY_CPP_SHARED_PTR(dog_t)
dog_t::clone() const
{
	BINARY_CPP_SHARED_PTR(dog_t)
	p;
	try
	{
		p = BINARY_CPP_MAKE_SHARED(dog_t);
		p->copy_from(*this);
	}
	BINARY_CPP_CATCH
	return p;
}
int dog_t::marshal_size(const binarycpp::context_t &ctx, int use) const
{
	use = binarycpp::marshal_size(ctx, use, id_);
	use = binarycpp::marshal_size(ctx, use, eat_);
	if (cat_)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
		use = cat_->marshal_size(ctx, use);
	}
	return use;
}
int dog_t::marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use) const
{
	use = binarycpp::marshal(ctx, buffer, capacity, use, 1, id_);
	use = binarycpp::marshal(ctx, buffer, capacity, use, 2, eat_);
	if (cat_)
	{
		use = binarycpp::marshal_type(ctx, buffer, capacity, use, 3, cat_.get());
	}
	return use;
}
void dog_t::unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n)
{
	reset();
	try
	{
		binarycpp::field_t fields[3];
		memset(fields, 0, sizeof(binarycpp::field_t) * 3);
		fields[0].id = 1;
		fields[1].id = 2;
		fields[2].id = 3;
		binarycpp::unmarshal_fields(ctx, input, n, fields, 3);
		if (fields[0].length)
		{
			binarycpp::unmarshal(ctx, fields[0], id_);
		}
		if (fields[1].length)
		{
			binarycpp::unmarshal(ctx, fields[1], eat_);
		}
		if (fields[2].length)
		{
			binarycpp::unmarshal_type(ctx, fields[2], cat_);
		}
	}
	BINARY_CPP_CATCH_RESET
}
fish_t::fish_t()
{
	reset();
}
fish_t::~fish_t()
{
}
void fish_t::reset()
{
}
void fish_t::swap(fish_t &other)
{
}
void fish_t::copy_from(const fish_t &other)
{
	try
	{
	}
	BINARY_CPP_CATCH_RESET
}
void fish_t::copy_from(const fish_t *other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
void fish_t::copy_from(const BINARY_CPP_SHARED_PTR(fish_t) & other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
BINARY_CPP_SHARED_PTR(fish_t)
fish_t::clone() const
{
	BINARY_CPP_SHARED_PTR(fish_t)
	p;
	try
	{
		p = BINARY_CPP_MAKE_SHARED(fish_t);
		p->copy_from(*this);
	}
	BINARY_CPP_CATCH
	return p;
}
int fish_t::marshal_size(const binarycpp::context_t &ctx, int use) const
{
	return use;
}
int fish_t::marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use) const
{
	return use;
}
void fish_t::unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n)
{
}
}; // namespace zoo::animal
