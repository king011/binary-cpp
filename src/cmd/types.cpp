#include "cmd.h"
#include "types.h"

void display(const std::vector<zoo::animal::pos_t> poss);
void display(const std::vector<BINARY_CPP_SHARED_PTR(zoo::animal::dog_t)> dogs);
void display(const std::vector<BINARY_CPP_SHARED_PTR(zoo::animal::cat_t)> cats);
void display(const zoo::animal::cat_t &cat, const char *prefix);
void display(const zoo::animal::dog_t &dog, const char *prefix);
void set_types(types::types_t &types)
{
    types.int16_ = -16;
    types.int32_ = -32;
    types.int64_ = -64;
    types.uint16_ = 16;
    types.uint32_ = 32;
    types.uint64_ = 64;
    types.float32_ = 32.32f;
    types.float64_ = 64.64;
    types.b_ = 8;
    types.ok_ = 1;
    types.str_ = "cerberus is an idea";
    // cat
    BINARY_CPP_SHARED_PTR(zoo::animal::cat_t)
    cat = BINARY_CPP_MAKE_SHARED(zoo::animal::cat_t);
    types.cat_ = cat;
    cat->name_ = "cat";

    BINARY_CPP_SHARED_PTR(zoo::animal::dog_t)
    dog = BINARY_CPP_MAKE_SHARED(zoo::animal::dog_t);
    dog->id_ = 2;
    cat->dog_ = dog;

    // dog
    dog = BINARY_CPP_MAKE_SHARED(zoo::animal::dog_t);
    types.dog_ = dog;
    dog->id_ = 1;
    dog->eat_.push_back("dog0");
    dog->eat_.push_back("dog1");
    cat = BINARY_CPP_MAKE_SHARED(zoo::animal::cat_t);
    cat->name_ = "dog->cat";
    dog->cat_ = cat;

    types.pos_ = zoo::animal::pos_t::cage;

    for (int i = 0; i < 16; i++)
    {
        types.int16s_.push_back((int16_t)(-16 + i));
    }
    for (int i = 0; i < 16; i++)
    {
        types.int32s_.push_back((int32_t)(-32 + i));
    }
    for (int i = 0; i < 16; i++)
    {
        types.int64s_.push_back((int64_t)(-48 + i));
    }
    for (int i = 0; i < 16; i++)
    {
        types.uint16s_.push_back((uint16_t)(i + 1));
    }
    for (int i = 0; i < 16; i++)
    {
        types.uint32s_.push_back((uint32_t)(i + 17));
    }
    for (int i = 0; i < 16; i++)
    {
        types.uint64s_.push_back((uint64_t)(i + 33));
    }

    types.float32s_.push_back(32.1);
    types.float32s_.push_back(32.2);

    types.float64s_.push_back(64.1);
    types.float64s_.push_back(64.2);
    for (int i = 0; i < 8; i++)
    {
        types.bs_.push_back((uint8_t)(i + 1));
    }
    for (int i = 0; i < 65; i++)
    {
        types.oks_.push_back(i % 2 != 0);
    }

    types.strs_.push_back("str0");
    types.strs_.push_back("");
    types.strs_.push_back("str1");

    // cats
    cat = BINARY_CPP_MAKE_SHARED(zoo::animal::cat_t);
    cat->name_ = "cat0";
    types.cats_.push_back(cat);
    cat = BINARY_CPP_MAKE_SHARED(zoo::animal::cat_t);
    cat->name_ = "cat1";
    types.cats_.push_back(cat);

    // dogs
    dog = BINARY_CPP_MAKE_SHARED(zoo::animal::dog_t);
    dog->id_ = 1;
    dog->eat_.push_back("dog0");
    dog->eat_.push_back("dog1");
    types.dogs_.push_back(dog);
    types.dogs_.push_back(NULL);

    dog = BINARY_CPP_MAKE_SHARED(zoo::animal::dog_t);
    dog->id_ = 2;
    dog->eat_.push_back("dog2");
    dog->eat_.push_back("dog3");
    types.dogs_.push_back(dog);

    dog = BINARY_CPP_MAKE_SHARED(zoo::animal::dog_t);
    types.dogs_.push_back(dog);

    types.poss_.push_back(zoo::animal::pos_t::corridor_p0);
    types.poss_.push_back(zoo::animal::pos_t::corridor_p1);
}
template <typename T>
void display_vector(const char *name, const std::vector<T> data)
{
    if (data.empty())
    {
        printf("%10s : \n", name);
    }
    else
    {
        printf("%10s : [ ", name);
        for (std::size_t i = 0; i < data.size(); i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << data[i];
        }
        puts(" ]");
    }
}
void display_vector(const char *name, const std::vector<std::uint8_t> data)
{
    if (data.empty())
    {
        printf("%10s : \n", name);
    }
    else
    {
        printf("%10s : [ ", name);
        for (std::size_t i = 0; i < data.size(); i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << (std::uint16_t)(data[i]);
        }
        puts(" ]");
    }
}
void display_vector(const char *name, const std::vector<bool> data)
{
    if (data.empty())
    {
        printf("%10s : \n", name);
    }
    else
    {
        printf("%10s : [ ", name);
        for (std::size_t i = 0; i < data.size(); i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            if (data[i])
            {
                std::cout << "true";
            }
            else
            {
                std::cout << "false";
            }
        }
        puts(" ]");
    }
}

void display(const std::vector<BINARY_CPP_SHARED_PTR(zoo::animal::cat_t)> cats)
{
    if (cats.empty())
    {
        printf("%10s : \n", "cats");
    }
    else
    {
        printf("%10s : [ ", "cats");
        for (std::size_t i = 0; i < cats.size(); i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            if (cats[i])
            {
                std::cout << cats[i]->name_;
            }
        }
        puts(" ]");
    }
}
void display(const std::vector<BINARY_CPP_SHARED_PTR(zoo::animal::dog_t)> dogs)
{
    if (dogs.empty())
    {
        printf("%10s : \n", "dogs");
    }
    else
    {
        printf("%10s : [ ", "dogs");
        for (std::size_t i = 0; i < dogs.size(); i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }

            std::cout << "{ ";
            if (dogs[i])
            {
                std::cout << dogs[i]->id_ << " : ";
                for (std::size_t j = 0; j < dogs[i]->eat_.size(); j++)
                {
                    if (j)
                    {
                        std::cout << " , ";
                    }
                    std::cout << dogs[i]->eat_[j];
                }
            }
            std::cout << " }";
        }
        puts(" ]");
    }
}
void display(const std::vector<zoo::animal::pos_t> poss)
{
    if (poss.empty())
    {
        printf("%10s : \n", "poss");
    }
    else
    {
        printf("%10s : [ ", "poss");
        for (std::size_t i = 0; i < poss.size(); i++)
        {
            if (i != 0)
            {
                std::cout << " , ";
            }
            std::cout << "{ "
                      << poss[i] << " : " << zoo::animal::enum_pos_string(poss[i])
                      << " }";
        }
        puts(" ]");
    }
}
void display(const zoo::animal::cat_t &cat, const char *prefix)
{
    if (prefix)
    {
        printf("%s%10s : ", prefix, "cat.name");
    }
    else
    {
        printf("%10s : ", "cat.name");
    }
    std::cout << cat.name_ << "\n";
    if (cat.dog_)
    {
        display(*cat.dog_, "cat-> ");
    }
}
void display(const zoo::animal::dog_t &dog, const char *prefix)
{
    if (prefix)
    {
        printf("%s %10s : %ld\n", prefix, "dog.id", dog.id_);
        printf("%s %10s : [", prefix, "dog.eat");
    }
    else
    {
        printf("%10s : %ld\n", "dog.id", dog.id_);
        printf("%10s : [", "dog.eat");
    }

    for (std::size_t i = 0; i < dog.eat_.size(); i++)
    {
        std::cout << i << "=" << dog.eat_[i] << ",";
    }

    printf("]\n");
    if (dog.cat_)
    {
        display(*dog.cat_, "dog-> ");
    }
}
void display(types::types_t &types)
{
    printf("%10s : %d\n", "int16", types.int16_);
    printf("%10s : %d\n", "int32", types.int32_);
    printf("%10s : %ld\n", "int64", types.int64_);
    printf("%10s : %d\n", "uint16", types.uint16_);
    printf("%10s : %d\n", "uint32", types.uint32_);
    printf("%10s : %ld\n", "uint64", types.uint64_);
    printf("%10s : %f\n", "float32", types.float32_);
    printf("%10s : %lf\n", "float64", types.float64_);
    printf("%10s : %d\n", "b", types.b_);
    printf("%10s : %d\n", "ok", types.ok_);
    printf("%10s : ", "str");
    std::cout << types.str_ << "\n";
    if (types.cat_)
    {
        display(*types.cat_, NULL);
    }
    if (types.dog_)
    {
        display(*types.dog_, NULL);
    }
    printf("%10s : ", "pos");
    std::cout << zoo::animal::enum_pos_string(types.pos_) << "\n";
    display_vector("int16s", types.int16s_);
    display_vector("int32s", types.int32s_);
    display_vector("int64s", types.int64s_);
    display_vector("uint16s", types.uint16s_);
    display_vector("uint32s", types.uint32s_);
    display_vector("uint64s", types.uint64s_);
    display_vector("float32s", types.float32s_);
    display_vector("float64s", types.float64s_);
    display_vector("bs", types.bs_);
    display_vector("oks", types.oks_);
    display_vector("strs", types.strs_);

    display(types.cats_);
    display(types.dogs_);
    display(types.poss_);
    puts("");
}
void subcommand_types(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("types", "test types");
    cmd->callback([&cmd = *cmd] {
        uint8_t *data = NULL;
        try
        {
            puts("***   reset   ***");
            types::types_t types;
            display(types);

            // set
            puts("***   set   ***");
            set_types(types);
            display(types);

            puts("***   marshal   ***");
            binarycpp::context_t ctx = binarycpp::background();
            std::size_t marshal_size = types.marshal_size(ctx);
            std::cout << " size = " << marshal_size << "\n";
            data = new uint8_t[marshal_size];

            int n = types.marshal(ctx, data, marshal_size);
            if (n != marshal_size)
            {
                std::cerr << "n != marshal_size " << n << std::endl;
                throw CLI::RuntimeError(n);
            }

            puts("***   reset   ***");
            types.reset();
            display(types);

            puts("***   unmarshal   ***");
            types.unmarshal(ctx, data, n);
            display(types);
        }
        catch (const std::exception &e)
        {
            if (data)
            {
                delete[] data;
            }
            std::cerr << e.what() << std::endl;
            throw CLI::RuntimeError(-1);
        }
        if (data)
        {
            delete[] data;
        }
        std::cout << "success" << std::endl;
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });
}
