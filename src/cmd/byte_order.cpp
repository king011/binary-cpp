#include "cmd.h"
#include <binary_cpp.h>
void subcommand_byte_order(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("byte_order", "byte_order");
    cmd->callback([&cmd = *cmd] {
        binarycpp::byte_order_t order;
        puts("***   big endian  ***");
        binarycpp::big_endian(order);
        {
            uint8_t data[2];
            for (uint16_t i = 1; i != 0; i++)
            {
                order.put_uint16(data, i);
                uint16_t dst = order.uint16(data);
                if (i != dst)
                {
                    std::cerr << "uint16 not equal : " << i << dst << std::endl;
                    throw CLI::RuntimeError(1);
                }
            }
        }
        {
            uint8_t data[4];
            uint32_t i = 0x01020304;
            order.put_uint32(data, i);
            printf("%d %d %d %d\n", data[0], data[1], data[2], data[3]);
            uint32_t dst = order.uint32(data);
            //    std::cout << std::hex << dst << std::endl;
            printf("\r%08x\n", i);
            if (i != dst)
            {
                std::cerr << "uint32 not equal : " << i << dst << std::endl;
                throw CLI::RuntimeError(1);
            }
        }
        {
            uint8_t data[8];
            uint64_t i = 0x0102030405060708;
            order.put_uint64(data, i);
            printf("%d %d %d %d %d %d %d %d\n", data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
            uint64_t dst = order.uint64(data);
            printf("\r%016lx\n", i);
            if (i != dst)
            {
                std::cerr << "uint64 not equal : " << i << dst << std::endl;
                throw CLI::RuntimeError(1);
            }
        }

        puts("***   little endian  ***");
        binarycpp::little_endian(order);
        {
            uint8_t data[2];
            for (uint16_t i = 1; i != 0; i++)
            {
                order.put_uint16(data, i);
                uint16_t dst = order.uint16(data);
                if (i != dst)
                {
                    std::cerr << "uint16 not equal : " << i << dst << std::endl;
                    throw CLI::RuntimeError(1);
                }
            }
        }
        {
            uint8_t data[4];
            uint32_t i = 0x01020304;
            order.put_uint32(data, i);
            printf("%d %d %d %d\n", data[0], data[1], data[2], data[3]);
            uint32_t dst = order.uint32(data);
            //    std::cout << std::hex << dst << std::endl;
            printf("\r%08x\n", i);
            if (i != dst)
            {
                std::cerr << "uint32 not equal : " << i << dst << std::endl;
                throw CLI::RuntimeError(1);
            }
        }
        {
            uint8_t data[8];
            uint64_t i = 0x0102030405060708;
            order.put_uint64(data, i);
            printf("%d %d %d %d %d %d %d %d\n", data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
            uint64_t dst = order.uint64(data);
            printf("\r%016lx\n", i);
            if (i != dst)
            {
                std::cerr << "uint64 not equal : " << i << dst << std::endl;
                throw CLI::RuntimeError(1);
            }
        }
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });
}