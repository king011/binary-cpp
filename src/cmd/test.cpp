#include "cmd.h"
#include <gtest/gtest.h>

void subcommand_test(CLI::App &app, char *path)
{
    CLI::App *cmd = app.add_subcommand("test", "run gtest");
    cmd->callback([&cmd = *cmd, path = path] {
        std::vector<std::string> strs = cmd.remaining();
        int argc = (int)strs.size() + 1;
        char **argv = NULL;
        argv = (char **)malloc(sizeof(char *) * argc);
        if (!argv)
        {
            throw CLI::RuntimeError(-1);
        }
        argv[0] = (char *)("app");
        for (size_t i = 0; i < strs.size(); i++)
        {
            argv[i] = (char *)strs[i + 1].c_str();
        }
        testing::InitGoogleTest(&argc, argv);
        free(argv);
        int result = RUN_ALL_TESTS();
        if (result)
        {
            throw CLI::RuntimeError(result);
        }
        else
        {
            throw CLI::Success();
        }
    });
    cmd->allow_extras();
}
