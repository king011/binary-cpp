#include <gtest/gtest.h>
#include <zoo/animal.h>

TEST(CatTest, HandleNoneZeroInput)
{
    binarycpp::context_t ctx = binarycpp::background();
    std::uint8_t data[1500];

    // base
    zoo::animal::cat_t cat;
    std::size_t size = cat.marshal_size(ctx);
    EXPECT_EQ(size, 0);
    EXPECT_TRUE(cat.name_.empty());

    std::string name = "this is cat";
    cat.name_ = name;
    size = cat.marshal_size(ctx);
    EXPECT_EQ(size, cat.name_.size() + 4);

    // marshal
    int n = cat.marshal(ctx, data, 1500);
    EXPECT_EQ(size, n);

    // unmarshal
    cat.reset();
    cat.unmarshal(ctx, data, n);
    EXPECT_EQ(cat.name_, name);
}

#define DOG_IS_RESET(dog)  \
    EXPECT_EQ(dog.id_, 0); \
    EXPECT_TRUE(dog.eat_.empty());
TEST(DogTest, HandleNoneZeroInput)
{
    binarycpp::context_t ctx = binarycpp::background();
    std::uint8_t data[1500];

    // base
    zoo::animal::dog_t dog;
    std::size_t size = dog.marshal_size(ctx);
    EXPECT_EQ(size, 0);
    DOG_IS_RESET(dog)

    // id
    int64_t id = std::rand() * std::rand();
    dog.id_ = id;
    size = dog.marshal_size(ctx);
    EXPECT_EQ(size, 2 + sizeof(dog.id_));
    int n = dog.marshal(ctx, data, 1500);
    EXPECT_EQ(size, n);
    dog.reset();
    DOG_IS_RESET(dog)

    dog.unmarshal(ctx, data, n);

    EXPECT_EQ(dog.id_, id);
    dog.reset();
    DOG_IS_RESET(dog)

    // eat
    std::vector<std::string> eat;
    eat.push_back("eat 0");
    eat.push_back("eat 1");
    dog.eat_ = eat;
    size = dog.marshal_size(ctx);
    EXPECT_EQ(size, 4 + 2 + eat[0].size() + 2 + eat[1].size());
    n = dog.marshal(ctx, data, 1500);
    EXPECT_EQ(n, size);

    dog.reset();
    DOG_IS_RESET(dog)
    dog.unmarshal(ctx, data, n);

    EXPECT_EQ(dog.id_, 0);
    EXPECT_EQ(dog.eat_.size(), 2);

    for (int i = 0; i < dog.eat_.size(); i++)
    {
        EXPECT_EQ(dog.eat_[i], eat[i]);
    }

    dog.reset();
    DOG_IS_RESET(dog)

    // id and eat
    id = std::rand() * std::rand();
    dog.id_ = id;
    dog.eat_ = eat;
    size = dog.marshal_size(ctx);
    EXPECT_EQ(size, 2 + sizeof(dog.id_) + 4 + 2 + eat[0].size() + 2 + eat[1].size());

    n = dog.marshal(ctx, data, 1500);
    EXPECT_EQ(size, n);
    dog.reset();
    DOG_IS_RESET(dog)
    dog.unmarshal(ctx, data, n);

    EXPECT_EQ(dog.id_, id);

    for (int i = 0; i < dog.eat_.size(); i++)
    {
        EXPECT_EQ(dog.eat_[i], eat[i]);
    }

    dog.reset();
    DOG_IS_RESET(dog)
}
