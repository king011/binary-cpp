#include <gtest/gtest.h>
#include "../types.h"
bool is_equal(const binarycpp::context_t &ctx, zoo::animal::cat_t *t0, zoo::animal::cat_t *t1)
{
    if (t0 && t1)
    {
        return t0->name_ == t1->name_;
    }

    return (!t0 || !(t0->marshal_size(ctx))) && (!t1 || !(t1->marshal_size(ctx)));
}
bool is_equal(const binarycpp::context_t &ctx, zoo::animal::dog_t *t0, zoo::animal::dog_t *t1)
{
    if (t0 && t1)
    {
        if (t0->eat_.size() != t1->eat_.size() || t0->id_ != t1->id_)
        {
            return false;
        }
        for (size_t i = 0; i < t0->eat_.size(); i++)
        {
            if (t0->eat_[i] != t1->eat_[i])
            {
                return false;
            }
        }
    }
    else
    {
        return (!t0 || !(t0->marshal_size(ctx))) && (!t1 || !(t1->marshal_size(ctx)));
    }
    return true;
}
#define TYPES_CHECK_ARRAY_EQUAL(t0, t1, name)      \
    for (size_t i = 0; i < t0.name##_.size(); i++) \
        if (t0.name##_[i] != t1.name##_[i])        \
            return false;

#define TYPES_NE_VAL(t0, t1, name) (t0.name##_ != t1.name##_)
bool is_equal(const binarycpp::context_t &ctx, types::types_t &t0, types::types_t &t1)
{

    if (TYPES_NE_VAL(t0, t1, int16) ||
        TYPES_NE_VAL(t0, t1, int32) ||
        TYPES_NE_VAL(t0, t1, int64) ||
        TYPES_NE_VAL(t0, t1, uint16) ||
        TYPES_NE_VAL(t0, t1, uint32) ||
        TYPES_NE_VAL(t0, t1, uint64) ||
        TYPES_NE_VAL(t0, t1, float32) ||
        TYPES_NE_VAL(t0, t1, float64) ||
        TYPES_NE_VAL(t0, t1, b) ||
        TYPES_NE_VAL(t0, t1, ok) ||
        TYPES_NE_VAL(t0, t1, str) ||
        !is_equal(ctx, t0.cat_.get(), t1.cat_.get()) ||
        !is_equal(ctx, t0.dog_.get(), t1.dog_.get()) ||
        TYPES_NE_VAL(t0, t1, pos) ||
        TYPES_NE_VAL(t0, t1, int16s) ||
        TYPES_NE_VAL(t0, t1, int32s) ||
        TYPES_NE_VAL(t0, t1, int64s) ||
        TYPES_NE_VAL(t0, t1, uint16s) ||
        TYPES_NE_VAL(t0, t1, uint32s) ||
        TYPES_NE_VAL(t0, t1, uint64s) ||
        TYPES_NE_VAL(t0, t1, float32s) ||
        TYPES_NE_VAL(t0, t1, float64s) ||
        TYPES_NE_VAL(t0, t1, bs) ||
        TYPES_NE_VAL(t0, t1, oks) ||
        TYPES_NE_VAL(t0, t1, strs) ||
        t0.cats_.size() != t1.cats_.size() ||
        t0.dogs_.size() != t1.dogs_.size() ||
        t0.poss_.size() != t1.poss_.size())
    {
        return false;
    }
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, int16s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, int32s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, int64s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, uint16s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, uint32s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, uint64s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, float32s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, float64s)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, bs)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, oks)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, strs)
    TYPES_CHECK_ARRAY_EQUAL(t0, t1, poss)

    for (size_t i = 0; i < t0.cats_.size(); i++)
    {
        if (!is_equal(ctx, t0.cats_[i].get(), t1.cats_[i].get()))
        {
            return false;
        }
    }

    for (size_t i = 0; i < t0.dogs_.size(); i++)
    {
        if (!is_equal(ctx, t0.dogs_[i].get(), t1.dogs_[i].get()))
        {
            return false;
        }
    }

    return true;
}

#define TYPES_IS_RESET(types)             \
    EXPECT_EQ(types.int16_, 0);           \
    EXPECT_EQ(types.int32_, 0);           \
    EXPECT_EQ(types.int64_, 0);           \
    EXPECT_EQ(types.uint16_, 0);          \
    EXPECT_EQ(types.uint32_, 0);          \
    EXPECT_EQ(types.uint64_, 0);          \
    EXPECT_EQ(types.float32_, 0);         \
    EXPECT_EQ(types.float64_, 0);         \
    EXPECT_EQ(types.b_, 0);               \
    EXPECT_EQ(types.ok_, 0);              \
    EXPECT_TRUE(types.str_.empty());      \
    EXPECT_FALSE(types.cat_);             \
    EXPECT_FALSE(types.dog_);             \
    EXPECT_EQ(types.pos_, 0);             \
    EXPECT_TRUE(types.int16s_.empty());   \
    EXPECT_TRUE(types.int32s_.empty());   \
    EXPECT_TRUE(types.int64s_.empty());   \
    EXPECT_TRUE(types.uint16s_.empty());  \
    EXPECT_TRUE(types.uint32s_.empty());  \
    EXPECT_TRUE(types.uint64s_.empty());  \
    EXPECT_TRUE(types.float32s_.empty()); \
    EXPECT_TRUE(types.float64s_.empty()); \
    EXPECT_TRUE(types.bs_.empty());       \
    EXPECT_TRUE(types.oks_.empty());      \
    EXPECT_TRUE(types.strs_.empty());     \
    EXPECT_TRUE(types.cats_.empty());     \
    EXPECT_TRUE(types.dogs_.empty());     \
    EXPECT_TRUE(types.poss_.empty());

TEST(TypesTest, HandleNoneZeroInput)
{
    binarycpp::context_t ctx = binarycpp::background();
    types::types_t types0,
        types1;
    TYPES_IS_RESET(types0)
    set_types(types0);

    TYPES_IS_RESET(types1)
    set_types(types1);

    // marshal
    int size = types0.marshal_size(ctx);
    uint8_t data[1500];
    int n = types0.marshal(ctx, data, 1500);
    EXPECT_EQ(size, n);

    types0.reset();
    TYPES_IS_RESET(types0)
    types0.unmarshal(ctx, data, n);

    EXPECT_TRUE(is_equal(ctx, types0, types1));

    // free
    types0.reset();
    TYPES_IS_RESET(types0)

    types1.reset();
    TYPES_IS_RESET(types1)
}