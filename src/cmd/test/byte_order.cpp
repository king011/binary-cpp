#include <gtest/gtest.h>
#include <binary_cpp.h>

TEST(ByteOrder16Test, HandleNoneZeroInput)
{

    binarycpp::byte_order_t order;
    binarycpp::big_endian(order);

    uint8_t data[2];
    uint16_t v = 0;
    order.put_uint16(data, v);
    uint16_t dst = order.uint16(data);
    EXPECT_EQ(v, dst);

    for (uint16_t v = 1; v != 0; v++)
    {
        order.put_uint16(data, v);
        uint16_t dst = order.uint16(data);
        EXPECT_EQ(v, dst);
    }

    binarycpp::little_endian(order);
    order.put_uint16(data, v);
    dst = order.uint16(data);
    EXPECT_EQ(v, dst);

    for (uint16_t v = 1; v != 0; v++)
    {
        order.put_uint16(data, v);
        uint16_t dst = order.uint16(data);
        EXPECT_EQ(v, dst);
    }
}
TEST(ByteOrder32Test, HandleNoneZeroInput)
{
    binarycpp::byte_order_t order;
    binarycpp::big_endian(order);

    uint8_t data[4];
    uint32_t v = 0;
    order.put_uint32(data, v);
    uint32_t dst = order.uint32(data);
    EXPECT_EQ(v, dst);

    v = 0x01020304;
    order.put_uint32(data, v);
    dst = order.uint32(data);
    EXPECT_EQ(v, dst);
    for (size_t i = 0; i < 10000; i++)
    {
        v = uint32_t(std::rand());
        order.put_uint32(data, v);
        dst = order.uint32(data);
        EXPECT_EQ(v, dst);
    }

    binarycpp::little_endian(order);
    v = 0;
    order.put_uint32(data, v);
    dst = order.uint32(data);
    EXPECT_EQ(v, dst);

    v = 0x01020304;
    order.put_uint32(data, v);
    dst = order.uint32(data);
    EXPECT_EQ(v, dst);
    for (size_t i = 0; i < 10000; i++)
    {
        v = uint32_t(std::rand());
        order.put_uint32(data, v);
        dst = order.uint32(data);
        EXPECT_EQ(v, dst);
    }
}
TEST(ByteOrder64Test, HandleNoneZeroInput)
{
    binarycpp::byte_order_t order;
    binarycpp::big_endian(order);

    uint8_t data[8];

    uint64_t v = 0;
    order.put_uint64(data, v);
    uint64_t dst = order.uint64(data);
    EXPECT_EQ(v, dst);

    v = 0x0102030405060708;
    order.put_uint64(data, v);
    dst = order.uint64(data);
    EXPECT_EQ(v, dst);
    for (size_t i = 0; i < 10000; i++)
    {
        v = uint64_t(std::rand()) * uint64_t(std::rand());
        order.put_uint64(data, v);
        dst = order.uint64(data);
        EXPECT_EQ(v, dst);
    }

    binarycpp::little_endian(order);
    v = 0;
    order.put_uint64(data, v);
    dst = order.uint64(data);
    EXPECT_EQ(v, dst);

    v = 0x0102030405060708;
    order.put_uint64(data, v);
    dst = order.uint64(data);
    EXPECT_EQ(v, dst);
    for (size_t i = 0; i < 10000; i++)
    {
        v = uint64_t(std::rand()) * uint64_t(std::rand());
        order.put_uint64(data, v);
        dst = order.uint64(data);
        EXPECT_EQ(v, dst);
    }
}