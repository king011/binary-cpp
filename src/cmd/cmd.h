#pragma once
#include "../CLI11.hpp"
void subcommand_test(CLI::App &app, char *path);
void subcommand_byte_order(CLI::App &app);
void subcommand_types(CLI::App &app);
void subcommand_example(CLI::App &app);
void subcommand_io(CLI::App &app);