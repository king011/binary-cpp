#include "cmd.h"

#include "./types.h"

void subcommand_io(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("io", "io test");
    cmd->callback([&cmd = *cmd] {
        try
        {
            const std::string w = cmd.get_option("--write")->as<std::string>();
            const std::string r = cmd.get_option("--read")->as<std::string>();
            if (!w.empty())
            {
                binarycpp::context_t ctx = binarycpp::background();
                types::types_t t;
                set_types(t);
                int marshal_size = t.marshal_size(ctx);
                boost::shared_array<uint8_t> data(new uint8_t[marshal_size]);
                t.marshal(ctx, data.get(), marshal_size);

                std::ofstream f(w.c_str(), std::ios::trunc | std::ios::out | std::ios::binary);
                f.write((const char *)data.get(), marshal_size);
                if (!f)
                {
                    std::cout << "write error" << std::endl;
                    throw CLI::RuntimeError(-1);
                }
            }
            else if (!r.empty())
            {
                puts("read");
                std::ifstream f(r.c_str(), std::ios::in | std::ios::binary);
                f.seekg(0, std::ios::end);
                int n = f.tellg();
                std::cout << n << "\n";
                boost::shared_array<uint8_t> data(new uint8_t[n]);
                f.seekg(0, std::ios::beg);
                f.read((char *)data.get(), n);

                binarycpp::context_t ctx = binarycpp::background();
                types::types_t t;
                t.unmarshal(ctx, data.get(), n);
                display(t);
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << std::endl;
            throw CLI::RuntimeError(-1);
        }
        throw CLI::Success();
    });
    static std::string w, r;
    cmd->add_option("-w,--write",
                    w,
                    "write file",
                    false);
    cmd->add_option("-r,--read",
                    r,
                    "read file",
                    false);
}